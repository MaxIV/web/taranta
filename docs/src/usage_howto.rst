How to use WebJive
******************



How to handle users
===================
.. todo::

    complete the description of the following things

    - how to configure them
    - how to enable/disable them
    - where to do that
    - how authentication is handled

\*Note: A token is created on successfull login of user which is valid for 1 hour. On expiry of this token ``You are logged out, please login to continue using dashboard.`` info message is shown to user and ``login`` link is visible in header section

 
How to use Devices
====================
.. todo::

    complete the description of the following things


    - how to change Tango server
    - the "Overview" section
    - how to search Devices
    - how to search for attributes and commands
    - how to monitor attributes and states
    - how to send commands and change attributes

How to use Dashboards
=====================

Creating a dashboard
---------------------

.. todo::

    complete the description of the following things

    - managing dashboards in the list of dashboards
    - addign widgets
    - say about the grid
    - say about layers

Running a dashboard
---------------------
.. todo::

    complete the descriptio


.. toctree::
    :maxdepth: 1

    errors-warnings

Using layers to create backgrounds
-----------------------------------
.. todo::

    Explain how to use layers to develop rich dashboard

Sharing dashboards
---------------------
.. todo::

    complete the description 


Exporting and importing dashboards
----------------------------------

.. toctree::
    :maxdepth: 1
    
    import_export_dashboard


Parametric Dashboard
----------------------------------

.. toctree::
    :maxdepth: 1
    
    dashboard_variables


