import React from "react";

import {
  AttributeInput
} from "../types";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import BooleanDisplay from "./BooleanDisplay";

type Input = {
  showDevice: boolean;
  attribute: AttributeInput;
};

configure({ adapter: new Adapter() });

describe("Boolean Display", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();
  let boolean_value = "DevBoolean";

  it("get a message if the value is not a DevBoolean", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevInt",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => { },
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("Selected attribute is different than boolean");
  });

  it("check if the attribute type is a boolean", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("input type=\"checkbox\""); 
  });

  it("set checked if the value is true", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("checked=\"\""); 
  });

  it("set checked if display device name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("sys/tg_test/1"); 
  });

  it("set checked if hide device name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1"); 
  });

  it("set showAttribute Label", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      label: "BOOLEAN_SCALAR",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("BOOLEAN_SCALAR"); 
  });

  it("set showAttribute Name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      label: "BOOLEAN_SCALAR",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "Name",
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("BOOLEAN_SCALAR"); 
    expect(shallow(element).html()).toContain("boolean_scalar"); 
  });

  it("set showAttribute None", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      label: "BOOLEAN_SCALAR",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "None",
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    const shallowElement = shallow(element);
    //console.log(shallowElement.instance().props);
    //console.log(shallowElement.instance().props.inputs.attribute.value);
    //shallowElement.state()['checkbox'] = !shallowElement.instance().props.inputs.attribute.value;

    expect(shallowElement.html()).not.toContain("BOOLEAN_SCALAR"); 
    expect(shallowElement.html()).not.toContain("boolean_scalar"); 
  });


  it("set checked pedint state on checkbox change", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      label: "BOOLEAN_SCALAR",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: false,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "None",
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });

    const shallowElement = shallow(element);
    let checkbox = shallowElement.find({ type: 'checkbox' });
    expect(shallowElement.state()).toMatchObject({ value: false, pending: false });
    shallowElement.state()['pending'] = true;
    //checkbox.simulate('change');
    expect(shallowElement.state()).toMatchObject({ value: false, pending: true });
  });

  it("pending false", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      label: "BOOLEAN_SCALAR",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => { },
      value: false,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "None",
      attribute: myAttributeInput
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });

    const shallowElement = shallow(element);
    shallowElement.state()['pending'] = false;
    let checkbox = shallowElement.find({ type: 'checkbox' });
    checkbox.simulate('change',{ target: { value: 'true' } });
  });

});
