import React, { Component, Fragment, CSSProperties } from "react";
import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  SelectInputDefinition
} from "../types";

import "./styles/BooleanDisplay.styles.css";
type Inputs = {
  showDevice: BooleanInputDefinition;
  attribute: AttributeInputDefinition;
  showAttribute: SelectInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  pending: boolean;
  value: number;
}

const style = { padding: "0.5em", whiteSpace: "nowrap" } as CSSProperties;
const styleCheckbox = { padding: "0.5em" } as CSSProperties;

class BooleanDisplay extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      value: this.attributeValue(),
      pending: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  public render() {
    const { device, name, label } = this.deviceAndAttribute();

    const value = this.attributeValue();
    if(this.state.value !== value) this.setState({ value: value });

    const { inputs, mode } = this.props;
    const isBoolean = inputs.attribute.dataType === "DevBoolean";
    let display = "";
    if(inputs.showAttribute === "Label") display = label;
    else if(inputs.showAttribute === "Name") display = name; 
    else if(inputs.showAttribute === "None") display = ""; 

    const deviceName = this.props.inputs.showDevice ? `${device}/${display}` : display;
  
    const Checkbox =
      mode !== "run" || isBoolean ? (
        <span style={styleCheckbox}>
          <label className="toggle">
            <input
              type="checkbox"
              checked={Boolean(this.state.value)}
              value = {this.state.value} 
              onChange= {this.handleChange}
            />
            <span className="slider round"></span>
          </label>
        </span>
      ) : (
        "Selected attribute is different than boolean"
      );

    return (
      <div style={style}>
        <Fragment>
          {deviceName} {Checkbox}
        </Fragment>
      </div>
    );
  }

  private attributeValue(): any {
    const {
      attribute: { value }
    } = this.props.inputs;
    return value;
  }

  private deviceAndAttribute(): { device: string; name: string, label: string } {
    const { attribute } = this.props.inputs;
    const device = attribute.device || "device";
    const name = attribute.attribute || "attribute";
    const label = attribute.label || "attributeLabel";
    return { device, name, label };
  }

  handleChange(event) {
    const { attribute } = this.props.inputs;
    const isBoolean = attribute.dataType === "DevBoolean";

    if (isBoolean) {
      const isChecked = this.attributeValue();
      this.props.inputs.attribute.write(!isChecked);
      this.setState({value: event.target.value});
    }
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "BOOLEAN_DISPLAY",
  name: "Boolean Display",
  defaultWidth: 10,
  defaultHeight: 2,
  inputs: {
    attribute: {
      type: "attribute",
      label: "",
      dataFormat: "scalar",
      required: true
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label"
        },
        {
          name: "Name",
          value: "Name"
        }
        ,
        {
          name: "None",
          value: "None"
        }
      ]
    },
    showDevice: {
      type: "boolean",
      label: "Device Name",
      default: false
    }
  }
};

export default { component: BooleanDisplay, definition };
