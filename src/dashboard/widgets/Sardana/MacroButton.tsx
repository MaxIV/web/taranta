import React, { Component } from "react";
import TangoAPI from "../../../shared/api/tangoAPI";

import {
  WidgetDefinition,
  DeviceInputDefinition,
  AttributeInputDefinition
} from "../../types";

import "./MacroButton.css";
import { StateIndicatorLabel } from "../../../shared/ui/components/StateIndicatorLabel";
import { WidgetProps } from "../types";
import { DeviceConsumer } from "../../components/DevicesProvider";
import { Button, Container, Row, Col } from "react-bootstrap";
import Tooltip from "react-tooltip-lite";
import cx from "classnames";

type Inputs = {
  door: DeviceInputDefinition;
  macroserver: DeviceInputDefinition;
  pool: DeviceInputDefinition;
  state: AttributeInputDefinition;
  macrolist: AttributeInputDefinition;
  motorlist: AttributeInputDefinition;
};

interface MacroArg {
  name: string;
  value: string;
  description: string;
  type: string; //Moveable, Float etc.
}

interface Macro {
  name: string;
  description: string;
}

interface State {
  selected_macro: Macro;
  macroArg: string;
  outputValue: string[];
  showModal: boolean;
  selected_macro_args: MacroArg[];
  toggle_macro_description: boolean;
}

interface TangoExecutionResult {
  ok: boolean;
  output: string[];
  message: string;
}

type Props = WidgetProps<Inputs>;

class MacroButton extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.handleMacro = this.handleMacro.bind(this);
    this.onChangeMacroArgValue = this.onChangeMacroArgValue.bind(this);
    this.state = {
      selected_macro: {
        description: "",
        name: ""
      },
      macroArg: "",
      outputValue: [],
      showModal: false,
      selected_macro_args: [],
      toggle_macro_description: false
    };
  }

  public render() {
    const { inputs, mode } = this.props;
    const { state, door, macrolist, motorlist } = inputs;

    let name = "";
    let alias = "";

    if (door) {
      name = door.name;
      alias = door.alias;
    }

    return (
      <div className="macro-button">
        <div className="door-row">
          <Container>
            <Row>
              <Col xs={5}>
                <StateIndicatorLabel
                  state={mode === "run" ? state.value : "UNKNOWN"}
                />
                <span className="device-alias">
                  {alias || name || "Device"}
                </span>
              </Col>
              <Col xs={1}>
                <Tooltip
                  content={`(${this.state.selected_macro.name}) ${this.state.selected_macro.description}`}
                  useDefaultStyles={true}
                  hoverDelay={0}
                  direction="left"
                  styles={{
                    marginLeft: "1.2rem",
                    width: "auto",
                    display:
                      this.state.selected_macro.description === ""
                        ? "none"
                        : "block"
                  }}
                >
                  <i
                    className={cx("DescriptionDisplay fa fa-info-circle")}
                    onClick={() =>
                      this.setState({
                        toggle_macro_description: true
                      })
                    }
                  />
                </Tooltip>
              </Col>
            </Row>
          </Container>
          <Container>
            <Row>
              <Col xs={6}>
                <select
                  className="form-control macro-list-dropdown full-width"
                  disabled={"library" === this.props.mode}
                  style={{
                    display: this.props.mode === "library" ? "block" : "none"
                  }}
                >
                  <option value="" disabled>
                    {macrolist.value === undefined
                      ? "No Macros available"
                      : "Select Macro"}
                  </option>
                </select>
              </Col>

              <Col xs={6}>
                <input
                  type="text"
                  className="form-control full-width"
                  style={{
                    borderTopLeftRadius: 4,
                    borderBottomLeftRadius: 4,
                    display: this.props.mode === "library" ? "block" : "none"
                  }}
                  placeholder={"Macro Args (separated by whitespace)"}
                />
              </Col>
            </Row>
          </Container>
          <div className="door-row">
            <div className="input-group">
              {mode === "run" && (
                <>
                  <DeviceConsumer>
                    {({ tangoDB }) => (
                      <>
                        <Container>
                          <Row>
                            <Col xs={6} style={{ paddingRight: "0" }}>
                              <select
                                className="form-control macro-list-dropdown"
                                value={this.state.selected_macro.name}
                                disabled={"library" === this.props.mode}
                                onChange={e => this.setMacro(e, tangoDB)}
                              >
                                <option value="" disabled>
                                  {macrolist.value === undefined
                                    ? "No Macros available"
                                    : "Select Macro"}
                                </option>
                                {macrolist.value &&
                                  macrolist.value.map(
                                    (macroItem: string, index: number) => {
                                      return (
                                        <option key={index} value={macroItem}>
                                          {macroItem}
                                        </option>
                                      );
                                    }
                                  )}
                              </select>
                            </Col>
                            <Col xs={6} style={{ paddingLeft: "0" }}>
                              <input
                                type="text"
                                className="form-control"
                                style={{
                                  borderTopLeftRadius: 4,
                                  borderBottomLeftRadius: 4
                                }}
                                value={this.state.macroArg}
                                readOnly={true}
                                placeholder={
                                  "Macro Args string will appear here"
                                }
                              />
                            </Col>
                          </Row>
                        </Container>

                        <Container>
                          <Row
                            style={{
                              display: `${
                                this.state.selected_macro_args.length === 0
                                  ? "none"
                                  : "flex"
                              }`
                            }}
                          >
                            {this.state.selected_macro_args.map(
                              (arg: MacroArg, index: number) => {
                                return arg.type === "Moveable" ? (
                                  <Col sm={4} key={index}>
                                    <select
                                      key={index}
                                      className="form-control"
                                      name={arg.name}
                                      value={arg.value}
                                      onChange={this.onChangeMacroArgValue}
                                      style={{ marginTop: "0.5rem" }}
                                    >
                                      <option value="" disabled>
                                        {motorlist.value.length === 0
                                          ? "No Motors available"
                                          : "Select Motor"}
                                      </option>
                                      {motorlist.value.map(
                                        (motor: string, index: number) => {
                                          return (
                                            <option key={index}>
                                              {JSON.parse(motor).name}
                                            </option>
                                          );
                                        }
                                      )}
                                    </select>
                                  </Col>
                                ) : (
                                  <Col sm={4} key={index}>
                                    <input
                                      key={index}
                                      type="text"
                                      className="form-control"
                                      style={{
                                        borderTopLeftRadius: 4,
                                        borderBottomLeftRadius: 4,
                                        marginTop: "0.5rem"
                                      }}
                                      placeholder={arg.name}
                                      onChange={this.onChangeMacroArgValue}
                                      name={arg.name}
                                      value={arg.value}
                                    />
                                  </Col>
                                );
                              }
                            )}
                          </Row>
                          <Row style={{ marginTop: "0.5rem" }}>
                            <Col sm={4}>
                              <Button
                                className="full-width form-control"
                                onClick={() =>
                                  this.handleMacro(
                                    tangoDB,
                                    "door",
                                    state.value === "RUNNING"
                                      ? "StopMacro"
                                      : "RunMacro"
                                  )
                                }
                                style={{ width: "100%" }}
                                disabled={this.state.selected_macro.name === ""}
                                size="lg"
                                variant={
                                  state.value === "RUNNING"
                                    ? "danger"
                                    : "success"
                                }
                              >
                                {state.value === "RUNNING" ? "Stop" : "Run"}
                              </Button>
                            </Col>
                          </Row>
                          {state.value === "RUNNING" &&
                            this.getOutputAttribute(tangoDB)}
                        </Container>
                      </>
                    )}
                  </DeviceConsumer>
                  <br />
                </>
              )}
            </div>
            <div className="result-section">
              <table className="result-table">
                <tbody>
                  {this.state.outputValue.length === 0 ? (
                    <tr>
                      <td style={{ color: "grey", marginBottom: "0" }}>
                        Door output will be displayed here
                      </td>
                    </tr>
                  ) : (
                    this.state.outputValue.map(
                      (singleStringRow: string, index: number) => {
                        return (
                          <tr key={index}>
                            <td>
                              <pre>{singleStringRow}</pre>
                            </td>
                          </tr>
                        );
                      }
                    )
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }

  onChangeMacroArgValue(event: any) {
    const { value, name } = event.target;
    const macro_args = this.state.selected_macro_args;
    macro_args[macro_args.findIndex(obj => obj.name === name)].value = value;

    // Concatinate all macro_args and display in readonly field
    let macro_arg_string = "";
    macro_args.forEach((arg: MacroArg) => {
      macro_arg_string += arg.value + " ";
    });
    macro_arg_string.trim();

    this.setState({
      selected_macro_args: macro_args,
      macroArg: macro_arg_string
    });
  }

  setMacroArg = (event: any) => {
    this.setState({ macroArg: event.target.value });
  };

  setMacro = (event: any, tangoDB: string) => {
    this.setState(
      {
        selected_macro: {
          description: "",
          name: event.target.value
        }
      },
      () => {
        this.executeCommand(tangoDB, "macroserver", "GetMacroInfo");
      }
    );
  };

  handleMacro(tangoDB: string, device: string, action: string) {
    this.executeCommand(tangoDB, device, action);
  }

  private getOutputAttribute(tangoDB: string) {
    const { door } = this.props.inputs;

    if (door) {
      TangoAPI.fetchAttributesValues(tangoDB, [door.name + "/Output"]).then(
        outputAttribute => {
          this.setState({
            outputValue: outputAttribute[0]
              ? outputAttribute[0].value
              : []
          });
        }
      );
    } else {
      alert("No door device");
    }
  }

  private async getOutputError(tangoDB: string) {
    const { door } = this.props.inputs;

    if (door) {
      const error = await TangoAPI.fetchAttributesValues(tangoDB, [
        door.name + "/Error"
      ]);
      return error[0].value ? error[0].value[0] : null;
    } else {
      alert("No door device");
    }
  }

  private async executeCommand(
    tangoDB: string,
    device: string,
    action: string
  ) {
    const { door, macroserver } = this.props.inputs;
    const { selected_macro, macroArg } = this.state;
    const argument =
      action === "RunMacro"
        ? Array(selected_macro.name).concat(macroArg.split(" "))
        : action === "GetMacroInfo"
        ? [selected_macro.name]
        : null;

    const deviceName = device === "door" ? door.name : macroserver.name;
    const selected_macro_args: MacroArg[] = [];

    const executionResult: TangoExecutionResult | null = await TangoAPI.executeCommand(
      tangoDB,
      deviceName,
      action,
      argument
    );
    let alertText: string | null = null;

    if (device === "macroserver" && executionResult) {
      let parameters = JSON.parse(executionResult.output[0]);
      console.log(parameters.description);
      for (let i = 0; i < parameters.parameters.length; i++) {
        let macro_arg_item: MacroArg = {
          description: parameters.parameters[i].description,
          name: parameters.parameters[i].name,
          value: "",
          type: parameters.parameters[i].type
        };
        selected_macro_args.push(macro_arg_item);
      }

      this.setState({
        selected_macro_args: selected_macro_args,
        selected_macro: {
          description: parameters.description.replace(/\s\s+/g, " "),
          name: selected_macro.name
        }
      });
    } else {
      // output error is only for door device
      const error = await this.getOutputError(tangoDB);
      if (error) {
        alertText = error;
      }
    }

    if (!executionResult || !executionResult.ok) {
      alertText = `There was a problem executing ${action}`;
    }
    alertText && alert(alertText);
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "MACROBUTTON",
  name: "Macro Button",
  defaultHeight: 12,
  defaultWidth: 32,
  inputs: {
    door: {
      type: "device",
      label: "Door",
      publish: "$door"
    },
    macroserver: {
      type: "device",
      label: "MacroServer",
      publish: "$macroserver"
    },
    pool: {
      type: "device",
      label: "Pool",
      publish: "$pool"
    },
    state: {
      type: "attribute",
      device: "$door",
      attribute: "state"
    },
    macrolist: {
      type: "attribute",
      device: "$macroserver",
      attribute: "macrolist",
      dataFormat: "spectrum"
    },
    motorlist: {
      type: "attribute",
      device: "$pool",
      attribute: "motorlist",
      dataFormat: "spectrum"
    }
  }
};

export default { definition, component: MacroButton };
