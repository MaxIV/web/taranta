import React from "react";
import { CommandInput } from "../types";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import CommandArray from "./CommandArray";

configure({ adapter: new Adapter() });

describe("CommandArray", () => {
  it("renders without crashing", () => {
    let commandInputArray: CommandInput;

    commandInputArray = {
      device: "sys/tg_test/1",
      command: "DevVarStringArray",
      output: "",
      execute: () => null,
    };

    const element = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "on",
        requireConfirmation: true,
        command: commandInputArray,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    element.props.inputs.command["acceptedType"] = "String";

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("OnCommand");
  });

  it("Show and hide title, widget, command, text button", () => {
    let commandInputArray: CommandInput;
    commandInputArray = {
      device: "sys/tg_test/1",
      command: "DevVarStringArray",
      output: "",
      execute: () => null,
    };

    let element = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "onCommandButton",
        requireConfirmation: true,
        command: commandInputArray,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    element.props.inputs.command["acceptedType"] = "String";

    let elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    //test if it shows device name and command
    expect(elementHtml).toContain("sys/tg_test/1/DevVarStringArray");
    //test if it shows button label
    expect(elementHtml).toContain("onCommandButton");

    element = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "",
        buttonText: "",
        requireConfirmation: true,
        command: commandInputArray,
        displayOutput: true,
        showDevice: true,
        showCommand: false,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    element.props.inputs.command["acceptedType"] = "String";

    elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    //test if it hides command label
    expect(elementHtml).not.toContain("sys/tg_test/1/DevVarStringArray");
    //test if it show only the device name
    expect(elementHtml).toContain("sys/tg_test/1:");
    //test if it hides button label
    expect(elementHtml).toContain("Submit");
    //test if it leaves title blank label
    expect(elementHtml).toContain(">sys/tg_test/1:<");
  });

  it("Show/hide input argument based on the type accepted from the command", () => {
    let commandInputArray: CommandInput;
    let commandInputVoid: CommandInput;
    let commandInputString: CommandInput;

    commandInputArray = {
      device: "sys/tg_test/1",
      command: "DevVarStringArray",
      output: "",
      execute: () => null,
    };

    commandInputVoid = {
      device: "sys/tg_test/1",
      command: "DevVoid",
      output: "",
      execute: () => null,
    };

    commandInputString = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null,
    };

    let elementVoid = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "on",
        requireConfirmation: true,
        command: commandInputVoid,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    elementVoid.props.inputs.command["acceptedType"] = "Void";

    let element = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "on",
        requireConfirmation: true,
        command: commandInputArray,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    element.props.inputs.command["acceptedType"] = "ArrayString";

    let elementString = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "on",
        requireConfirmation: true,
        command: commandInputString,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    elementString.props.inputs.command["acceptedType"] = "String";

    let elementVoidHtml = shallow(elementVoid)
      .html()
      .replace(/\s/g, "");
    let elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    let elementStringHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    //test if it shows the input argument if the acceptedType is an array
    expect(elementHtml).not.toContain("hidden");
    //test if it shows the input argument if the acceptedType is a string
    expect(elementStringHtml).not.toContain("hidden");
  });

  it("Widget on widget selection display", () => {
    let commandInputVoid: CommandInput;

    commandInputVoid = {
      device: "",
      command: "",
      output: "",
      execute: () => null,
    };

    let elementVoid = React.createElement(CommandArray.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "on",
        requireConfirmation: true,
        command: commandInputVoid,
        displayOutput: true,
        showDevice: false,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    expect(shallow(elementVoid).html()).toContain("On Command");
  });

  it("Widget command execute no confirmation", () => {
    let commandInputString: CommandInput;

    commandInputString = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "Hello",
      dataType: "DevString",
      execute: () => jest.fn(),
    };

    let elementString = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "Submit",
        requireConfirmation: false,
        command: commandInputString,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    var commandArrayWidget = mount(elementString);
    var input = commandArrayWidget.find("input");
    input.simulate("change", { target: { value: "Hello" } });
    commandArrayWidget.find("button").simulate("click");
    expect(commandArrayWidget.html()).toContain("table");
  });

  it("Widget command execute with state pending", () => {
    let commandInputString: CommandInput;

    commandInputString = {
      device: "sys/tg_test/1",
      command: "DevString",
      dataType: "DevString",
      output: "Hello",
      execute: () => jest.fn(),
    };

    let elementString = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "Submit",
        requireConfirmation: true,
        command: commandInputString,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    var commandArrayWidget = mount(elementString);
    var input = commandArrayWidget.find("input");
    commandArrayWidget.state()["pending"] = true;
    input.simulate("change", { target: { value: "Hello" } });
    commandArrayWidget.find("button").simulate("click");
    expect(commandArrayWidget.html()).toContain("table");
  });

  it("Widget command execute with state pending", () => {
    let commandInputString: CommandInput;

    commandInputString = {
      device: "sys/tg_test/1",
      command: "DevString",
      dataType: "DevString",
      output: "Hello",
      execute: () => jest.fn(),
    };

    let elementString = React.createElement(CommandArray.component, {
      mode: "run",
      t0: 1,
      id: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "Submit",
        requireConfirmation: false,
        command: commandInputString,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
      },
    });

    var commandArrayWidget = mount(elementString);
    var input = commandArrayWidget.find("input");
    commandArrayWidget.state()["pending"] = true;
    commandArrayWidget.props().inputs.command["acceptedType"] = "DevString";
    input.simulate("change", { target: { value: "Hello" } });
    commandArrayWidget.find("button").simulate("click");

    expect(commandArrayWidget.html()).toContain("table");
  });

  it("Widget command execute no confirmation view more", () => {
    jest.useFakeTimers();

    let commandInputString: CommandInput;

    commandInputString = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "Hello",
      execute: () => jest.fn(),
    };

    let elementString = React.createElement(CommandArray.component, {
      mode: "run",
      id: 1,
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: "On Command",
        buttonText: "Submit",
        requireConfirmation: false,
        command: commandInputString,
        displayOutput: true,
        showDevice: true,
        showCommand: true,
        textColor: "black",
        backgroundColor: "white",
        size: 1.7,
        font: "Helvetica",
        OutputMaxSize: 2,
        timeDisplayOutput: 100,
      },
    });

    var commandArrayWidget = mount(elementString);
    var input = commandArrayWidget.find("input");
    input.simulate("change", { target: { value: "Hello" } });
    commandArrayWidget.find("button").simulate("click");
    expect(commandArrayWidget.html()).toContain("table");
    expect(commandArrayWidget.html()).toContain("View More");

    jest.runAllTimers();

    commandArrayWidget.find("#viewmore").simulate("click");
    expect(commandArrayWidget.html()).toContain("He...");
  });
});
