import { getDashboardVariables, getDeviceOfDashboardVariable, mapVariableNameToDevice, getRunningClasses, getRunningDevices, checkVariableDevice, checkVariableConsistency, variablePresent } from './DashboardVariables';

let dashboardVariables =  JSON.parse('[{ "id": "d5jeieka9dgg", "name": "var2", "class": "TangoTest", "device": "sys/tg_test/1" }, { "id": "kihdc2dgelha", "name": "var1", "class": "DataBase", "device": "sys/database/2" }]'); 
let tangoClasses = [
    {
        "name": "DataBase",
        "devices": [
            {
                "name": "sys/database/2",
                "exported": true,
                "connected": true
            }
        ]
    },
    {
        "name": "TangoTest",
        "devices": [
            {
                "name": "sys/tg_test/1",
                "exported": true,
                "connected": false
            }
        ]
    }
]

const tangoClass = "TangoTest"
const tangoDevice = "sys/tg_test/1"
const invalidDevice = "sys/invalid_Device/1"

describe("test util Dashboard Variable functions", () =>{

  const dashboards = [{id:'604b5fac8755940011efeea1',name:'Untitled dashboard',user:'user1',insertTime:'2021-03-12T12:33:48.981Z',updateTime:'2021-04-20T08:12:53.689Z',group:null,lastUpdatedBy:'user1',tangoDB:'testdb',variables:[{id:'0698hln1j359a',name:'myvar','class':'tg_test',device:'sys/tg_test/1'}]},{id:'6073154703c08b0018111066',name:'Untitled dashboard',user:'user1',insertTime:'2021-04-11T15:27:03.803Z',updateTime:'2021-04-20T08:44:21.129Z',group:null,lastUpdatedBy:'user1',tangoDB:'testdb',variables:[{id:'78l4d190kh2g',name:'cspvar1','class':'tg_test',device:'sys/tg_test/1'},{id:'i5am90g1il0e',name:'myvar','class':'tg_test',device:'sys/tg_test/1'}]}];
  const selectedDashboardId = '6073154703c08b0018111066';
  const widgets = [
    {
      _id: '607e8e9c99ba3e0017cf9080',
      id: '1',
      x: 16,
      y: 5,
      canvas: '0',
      width: 14,
      height: 3,
      type: 'ATTRIBUTE_DISPLAY',
      inputs: {
        attribute: {
          device: 'CSPVar1',
          attribute: 'state',
          label: 'State'
        },
        precision: 2,
        showDevice: false,
        showAttribute: 'Label',
        scientificNotation: false,
        showEnumLabels: false,
        textColor: '#000000',
        backgroundColor: '#ffffff',
        size: 1,
        font: 'Helvetica'
      },
      order: 0,
      valid: true
    },
    {
      _id: '607e946599ba3e0017cf90a0',
      id: '2',
      x: 16,
      y: 10,
      canvas: '0',
      width: 20,
      height: 3,
      type: 'COMMAND_ARRAY',
      inputs: {
        command: {
          device: 'myvar',
          command: 'DevString',
          acceptedType: 'DevString'
        },
        showDevice: true,
        showCommand: true,
        requireConfirmation: true,
        displayOutput: true,
        OutputMaxSize: 20,
        timeDisplayOutput: 3000,
        textColor: '#000000',
        backgroundColor: '#ffffff',
        size: 1,
        font: 'Helvetica'
      },
      order: 1,
      valid: true
    }
  ];

  const variables = [
    {id: '11', 'name': 'CSPVar1', class: 'tg_test', device: 'dserver/databaseds/2' },
    {id: '12', 'name': 'TMCVar2', class: 'tarantatestdevice', device: 'tarantatestdevice2'},
  ];

  it("test if dashboard variables are replaced by default define in subscribed widgets", () => {
    expect(getDashboardVariables('', dashboards)).toEqual([]);
    expect(JSON.stringify(mapVariableNameToDevice(widgets, selectedDashboardId, dashboards))).toContain('sys/tg_test/1');
    expect(JSON.stringify(mapVariableNameToDevice(widgets, selectedDashboardId, dashboards))).not.toContain('myvar');
    expect(JSON.stringify(mapVariableNameToDevice(widgets, selectedDashboardId, dashboards))).not.toContain('cspvar1');
    expect(JSON.stringify(mapVariableNameToDevice(widgets, '', dashboards))).toEqual(JSON.stringify(widgets));
  });

  it("test if dashboard variables are searched correctly", ()=>{
    expect(getDeviceOfDashboardVariable(variables, 'CSPVar1')).toContain('dserver/databaseds/2');
    expect(getDeviceOfDashboardVariable(variables, 'CSPVa22')).toContain('CSPVa22');
  });

  it("test getRunningClasses", () => {
        
    //connect the second device
    tangoClasses[1].devices[0].connected = true
    expect(Object.values(getRunningClasses(tangoClasses)).length).toBe(2)

    //disconnect the second device
    tangoClasses[1].devices[0].connected = false
    expect(Object.values(getRunningClasses(tangoClasses)).length).toBe(1)

})

it("test getRunningDevices", () => {
    
    //connect TangoTestDevice
    let tangoDevices = [
      {name: 'sys/tg_test/1', exported: true, connected: true}
    ];

    expect(Object.values(getRunningDevices(tangoDevices)).length).toBe(1) 
    expect(Object.values(getRunningDevices(tangoDevices))[0].name).toBe(tangoDevice) 

    tangoDevices[0].connected = false;
    expect(Object.values(getRunningDevices(tangoClasses, tangoClass)).length).toBe(0) 
})

it("test checkVariableDevice", () => {

    let tangoDevices = [
      {name: 'sys/tg_test/1', exported: true, connected: true}
    ];

    let result = checkVariableDevice("sys/tg_test/1",tangoDevices)
    expect(result.connected).toBe(true)
    expect(result.exported).toBe(true)

    //disconnect TangoTestDevice
    tangoDevices[0].connected = false
    result = checkVariableDevice("sys/tg_test/1",tangoDevices)
    expect(result.connected).toBe(false)
    expect(result.exported).toBe(true)

    //test invalid device
    result = checkVariableDevice("not found",tangoDevices)
    expect(result.connected).toBe(false)
    expect(result.exported).toBe(false)
})


it("test if dashboard variables are present", ()=>{
    expect(variablePresent(widgets[0], variables)).toEqual({found: true, device: 'CSPVar1'});
    expect(variablePresent(widgets[1], variables)).toEqual({found: false, device: 'myvar'});
    expect(variablePresent(widgets[1], undefined)).toEqual({found: true, device: ''});

    let wid = {
      "_id": "609bb1c91233730011f37222",
      "id": "4",
      "x": 17,
      "y": 7,
      "canvas": "0",
      "width": 14,
      "height": 3,
      "type": "DEVICE_STATUS",
      "inputs": {
          "device": "CSPVar1",
          "state": {
              "device": null,
              "attribute": null
          },
          "showDeviceName": true,
          "showStateString": true,
          "showStateLED": true,
          "LEDSize": 1,
          "textSize": 1
      },
      "order": 3,
      "valid": true
    }
    expect(variablePresent(wid, variables)).toEqual({found: true, device: 'CSPVar1'});
    wid.inputs.device = 'myvar';
    expect(variablePresent(widgets[1], variables)).toEqual({found: false, device: 'myvar'});

    wid = {
      "_id": "60a65cbc5b214d00172b4940",
      "id": "1",
      "x": 15,
      "y": 5,
      "canvas": "0",
      "width": 30,
      "height": 20,
      "type": "TIMELINE",
      "inputs": {
          "timeWindow": 120,
          "overflow": false,
          "groupAttributes": false,
          "attributes": [
              {
                  "attribute": {
                      "device": "CSPVar1",
                      "attribute": "state",
                      "label": "State"
                  },
                  "yAxisDisplay": "Label",
                  "showAttribute": "Label",
                  "yAxis": "left"
              }
          ]
      },
      "order": 0,
      "valid": true
    }
    expect(variablePresent(wid, variables)).toEqual({found: true, device: ''});
    wid.inputs.attributes[0].attribute.device = 'myvar';
    expect(variablePresent(wid, variables)).toEqual({found: false, device: 'myvar'});

  });
});
